package com.almundo.callcenter;

public class Employee {
	
	private EmpoyeeRole role;
	private String name;
	
	public Employee(EmpoyeeRole role, String name) {
		this.role = role;
		this.name = name;
	}
	
	public String getName() { return this.name; }
	public EmpoyeeRole getRole() { return this.role; }
}
