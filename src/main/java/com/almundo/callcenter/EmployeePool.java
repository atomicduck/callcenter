package com.almundo.callcenter;

import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;

public class EmployeePool {
	private PriorityBlockingQueue<Employee> employees;

	public EmployeePool(Comparator<Employee> comparator) {
		this.employees = new PriorityBlockingQueue<Employee>(10, comparator);
	}
	
	public int getAvailableEmployees() {
		return employees.size();
	}
	
	public Employee pull() throws InterruptedException {
		return this.employees.take();
	}
	
	public void push(Employee emp) {
		employees.add(emp);
	}
}
