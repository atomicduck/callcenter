package com.almundo.callcenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Dispatcher {
	private EmployeePool employeePool;
	
	private Executor executor;
	private List<Call> callQueue;
	
	public Dispatcher(EmployeePool employeePool, int maxConcurrentCalls){
		this.employeePool = employeePool;
		this.executor = Executors.newFixedThreadPool(maxConcurrentCalls);
		this.callQueue = Collections.synchronizedList(new ArrayList<Call>());
	}
	
	public CallQueue dispatchCall(Call call) throws Exception {
		callQueue.add(call);
		return new CallQueue(call,
		CompletableFuture.supplyAsync(()->{
				
				Employee emp;
				try {
					emp = employeePool.pull();
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
				callQueue.remove(call);
				call.start(emp);
				employeePool.push(emp);
				
				return call;
			}, executor));
	}

	public class CallQueue {
		private Call call;
		private CompletableFuture<Call> task;
		
		private CallQueue(Call call, CompletableFuture<Call> task) {
			this.call = call;
			this.task = task;
		}
		
		public int getQueuePosition() {
			return callQueue.indexOf(call);
		}
		
		public Call getCall() {
			return call;
		}
		
		public boolean dequeue() {
			if(callQueue.remove(call)) {
				task.cancel(false);
				return true;
			}
			return false;
		}
		
		public Call waitCall() throws InterruptedException, ExecutionException {
			return task.get();
		}
	}
}
