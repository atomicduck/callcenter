package com.almundo.callcenter;

import java.util.Comparator;
import java.util.HashMap;

public class CallCenterPriorities {
	public static Comparator<Employee> roleBasedPriority() {
		HashMap<EmpoyeeRole, Integer> rolePriority = new HashMap<EmpoyeeRole, Integer>();
    	rolePriority.put(EmpoyeeRole.Operator, 1);
    	rolePriority.put(EmpoyeeRole.Supervisor, 2);
    	rolePriority.put(EmpoyeeRole.Director, 3);
    	
		return new Comparator<Employee>() {
				@Override
				public int compare(Employee o1, Employee o2) {
					return rolePriority.get(o1.getRole()) - rolePriority.get(o2.getRole());
				}
			};
	}
}
