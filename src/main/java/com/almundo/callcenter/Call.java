package com.almundo.callcenter;

import java.util.Random;

public class Call {
	
	public enum Status{
		OPEN,CANCELED,FINISHED,STARTED
	}
	
	private Status state = Status.OPEN;
	private Employee employee;
	private Random rnd = new Random();
	
	public void start(Employee emp)  {
		try {
			this.employee = emp;
			setState(Status.STARTED);
			Thread.sleep(5000 + rnd.nextInt(5000));
			finish();
		} catch (InterruptedException e) {
		}
	}
	
	public Employee getEmployee() {
		return employee;
	}
	
	public void finish() {
		setState(Status.FINISHED);
	}
	
	public void cancel() {
		setState(Status.CANCELED);
	}
	
	public Status getState() {
		return state;
	}
	private void setState(Status state){
		this.state = state;
	}
}
