package com.almundo.callcenter;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.almundo.callcenter.Call;
import com.almundo.callcenter.Call.Status;
import com.almundo.callcenter.CallCenterPriorities;
import com.almundo.callcenter.Dispatcher;
import com.almundo.callcenter.Employee;
import com.almundo.callcenter.EmployeePool;
import com.almundo.callcenter.EmpoyeeRole;
import com.almundo.callcenter.Dispatcher.CallQueue;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple callCenter.
 */
public class CallCenterTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public CallCenterTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( CallCenterTest.class );
    }

    public void testCallDispatcher() throws InterruptedException, ExecutionException, Exception
    {
    	EmployeePool employeePool = new EmployeePool(CallCenterPriorities.roleBasedPriority());
    	
    	employeePool.push(new Employee(EmpoyeeRole.Operator, "operator1"));
    	employeePool.push(new Employee(EmpoyeeRole.Supervisor, "supervisor1"));
    	employeePool.push(new Employee(EmpoyeeRole.Director, "director1"));
    	
    	Dispatcher dispatcher = new Dispatcher(employeePool, 10);
        
    	ArrayList<Call> calls = new ArrayList<Call>();
    	for(int i =0 ;i<10;i++) {
    		calls.add(new Call());
    	}
    	calls.parallelStream().forEach((c)->{
			try {
				dispatcher.dispatchCall(c).waitCall();
			} catch (Exception e) {
				throw new AssertionError(e);
			}
		});
        
    	calls.forEach((c)->assertTrue(c.getState() == Status.FINISHED) );
        
    }

    public void testNoAvailableEmployee() throws InterruptedException, ExecutionException, Exception
    {
    	EmployeePool employeePool = new EmployeePool(CallCenterPriorities.roleBasedPriority());
    	
    	employeePool.push(new Employee(EmpoyeeRole.Operator, "operator1"));
    	
    	Dispatcher dispatcher = new Dispatcher(employeePool, 10);
        
    	dispatcher.dispatchCall(new Call());
    	CallQueue q = dispatcher.dispatchCall(new Call());
    	
    	assertTrue(q.getQueuePosition()>=0);
    	assertTrue(q.waitCall().getState() == Status.FINISHED);   
    }
    
    public void testMaxCallsReached() throws InterruptedException, ExecutionException, Exception
    {
    	EmployeePool employeePool = new EmployeePool(CallCenterPriorities.roleBasedPriority());
    	
    	employeePool.push(new Employee(EmpoyeeRole.Operator, "operator1"));
    	employeePool.push(new Employee(EmpoyeeRole.Supervisor, "supervisor1"));
    	
    	Dispatcher dispatcher = new Dispatcher(employeePool, 1);
        
    	dispatcher.dispatchCall(new Call());
    	CallQueue q = dispatcher.dispatchCall(new Call());
    	
    	assertTrue(q.getQueuePosition()>=0);
    	assertTrue(q.waitCall().getState() == Status.FINISHED);        
    }
    
    public void testUnqueueCall() throws InterruptedException, ExecutionException, Exception
    {
    	EmployeePool employeePool = new EmployeePool(CallCenterPriorities.roleBasedPriority());
    	
    	employeePool.push(new Employee(EmpoyeeRole.Operator, "operator1"));
    	employeePool.push(new Employee(EmpoyeeRole.Supervisor, "supervisor1"));
    	
    	Dispatcher dispatcher = new Dispatcher(employeePool, 1);
        
    	dispatcher.dispatchCall(new Call());
    	CallQueue q = dispatcher.dispatchCall(new Call());

    	assertTrue(q.getQueuePosition()>=0);
    	assertTrue(q.dequeue());        
    }
    
    public void testPriorityCall() throws InterruptedException, ExecutionException, Exception
    {
    	EmployeePool employeePool = new EmployeePool(CallCenterPriorities.roleBasedPriority());
    	
    	employeePool.push(new Employee(EmpoyeeRole.Operator, "operator1"));
    	employeePool.push(new Employee(EmpoyeeRole.Supervisor, "supervisor1"));
    	employeePool.push(new Employee(EmpoyeeRole.Director, "director1"));
    	
    	Dispatcher dispatcher = new Dispatcher(employeePool, 3);
        
    	
    	CallQueue q1 = dispatcher.dispatchCall(new Call());
    	CallQueue q2 = dispatcher.dispatchCall(new Call());
    	CallQueue q3 = dispatcher.dispatchCall(new Call());

    	assertTrue(q1.waitCall().getEmployee().getRole() == EmpoyeeRole.Operator);
    	assertTrue(q2.waitCall().getEmployee().getRole() == EmpoyeeRole.Supervisor);
    	assertTrue(q3.waitCall().getEmployee().getRole() == EmpoyeeRole.Director);
    }
}
